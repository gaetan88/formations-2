package main;

public class URLS {

	public static String HELLO = "http://localhost:8080/TP/jersey/agent/hello";
	public static String AGENT = "http://localhost:8080/TP/jersey/agent";
	public static String AGENT_BY_IDEP = "http://localhost:8080/TP/jersey/agent/{idep}";
	public static String AGENTS_BY_FILTRE = "http://localhost:8080/TP/jersey/agent/filtre";
	public static String ADD_AGENT = "http://localhost:8080/TP/jersey/agent/ajouter";
}
