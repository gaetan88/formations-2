package csvparser;

import java.io.InputStream;
import java.util.List;

public interface CSVParser<T> {

	public List<T> parseStream(InputStream stream);
	
}
