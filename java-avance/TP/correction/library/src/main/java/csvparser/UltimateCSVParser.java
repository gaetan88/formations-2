package csvparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UltimateCSVParser<T> implements CSVParser<T> {

	@Override
	public List<T> parseStream(InputStream stream) {
		List<T> liste = new ArrayList<T>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			String line = null;
			String header = reader.readLine();
			handleHeader(header);
			while ((line = reader.readLine()) != null) {
				T object = handleLine(line);
				liste.add(object);
			}
		}
		catch (IOException e) {
			
		}
		
		return liste;
	}
	
	private void handleHeader(String header) {
		
	}
	
	private T handleLine(String line) {
		try {
			T object = (T) getClass().getTypeParameters()[0].getGenericDeclaration().newInstance();
			return object;
		}
		catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

}
